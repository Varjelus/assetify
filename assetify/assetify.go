package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/dave/jennifer/jen"
	"gitlab.com/Varjelus/assetify"
)

var (
	input         string
	targetPackage string
	assetName     string
)

func init() {
	flag.StringVar(&input, "i", "", "Input file path")
	flag.StringVar(&targetPackage, "p", "main", "Target package name")
	flag.StringVar(&assetName, "n", "", "Variable name you want this asset to be available")
	flag.Parse()
}

func main() {
	if len(input) == 0 {
		flag.PrintDefaults()
		os.Exit(0)
	}

	// Decide on asset name
	if assetName == "" {
		base := filepath.Base(input)
		assetName = base[:len(base)-len(filepath.Ext(base))]
	}

	// Open asset for reading
	fileHandler, err := os.Open(input)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer fileHandler.Close()

	// Compress asset
	data := assetify.New()
	if _, err := io.Copy(data, fileHandler); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// Generate code
	var j []jen.Code
	for _, b := range data.Bytes() {
		j = append(j, jen.Lit(int(b)))
	}
	output := jen.NewFile(targetPackage)
	output.Comment("This file has been automatically generated with gitlab.com/Varjelus/assetify")
	output.Var().Id(assetName).Op("*").Qual("gitlab.com/Varjelus/assetify", "Data")
	output.Func().Id("init").Params().Block(
		jen.Id(assetName).Op("=").Qual("gitlab.com/Varjelus/assetify", "New").Call(),
		jen.List(jen.Id("_"), jen.Err()).Op(":=").Id(assetName).Dot("Write").Call(jen.Index().Byte().Values(j...)),
		jen.If(jen.Id("err").Op("!=").Nil()).Block(
			jen.Panic(jen.Id("err")),
		),
	)

	// Write .go file here
	if err := output.Save(fmt.Sprintf("%s.go", assetName)); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println("Success")
}
