package assetify

import (
	"bytes"
	"compress/gzip"
	"io"
)

/* Data implements `io.Reader` and `io.Writer`, internally compressing
and decompressing bytes with `compress/gzip`. */
type Data struct {
	r   *gzip.Reader
	w   *gzip.Writer
	buf *bytes.Buffer
}

// New initializes a new `Data` instance.
func New() *Data {
	d := &Data{}
	d.buf = new(bytes.Buffer)
	return d
}

// Bytes returns the _compressed_ bytes stored in `Data`.
func (d *Data) Bytes() []byte {
	return d.buf.Bytes()
}

// Read inflates stored data and inserts them to *p*.
func (d *Data) Read(p []byte) (n int, err error) {
	if len(d.buf.Bytes()) == 0 {
		err = io.EOF
		return
	}
	if d.r == nil {
		d.r, err = gzip.NewReader(d.buf)
		if err != nil {
			return
		}
	}
	return d.r.Read(p)
}

// Write compresses *p* and stores the data.
func (d *Data) Write(p []byte) (n int, err error) {
	if d.w == nil {
		d.w, err = gzip.NewWriterLevel(d.buf, gzip.BestCompression)
		if err != nil {
			return
		}
	}
	n, err = d.w.Write(p)
	if err != nil {
		return
	}
	err = d.w.Flush()
	return
}

// Reset reinitializes `Data`, losing stored information.
func (d *Data) Reset() {
	d.w.Reset(d.w)
	d.r.Close()
	d.buf = new(bytes.Buffer)
	d.r = nil
}

// Close calls `Data.Reset` and closes internal readers and writers.
func (d *Data) Close() error {
	d.Reset()
	return d.w.Close()
}
